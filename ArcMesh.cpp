#include "stdafx.h"
#include "ArcMesh.h"

ArcMesh::ArcMesh(const size_t& subdiv_level, const double& west, const double& east, const double& south, const double& north) : SubdivisionLevel(subdiv_level), 
    WesternLimit(west), EasternLimit(east), SouthernLimit(south), NorthernLimit(north), Position(0.0f), Scale(1.0f), Angle(0.0f) {

        // Check input latt/long (first check)
        if(north <= south || east <= west){
            std::cerr << "ArcMesh::ArcMesh::L9: The geographic bounds are not correctly specified relative to each other " << std::endl;
            throw;
        }

        // Check input latt/long again to make sure they aren't out-of-bounds of a globe
        if(north > 90.0 || south < -90.0 || east > 180.0 || west < -180.0){
            std::cerr << "ArcMesh::ArcMesh::L14: One of the supplied geographic bounds is outside of the possible boundaries for a sphere! " << std::endl;
            throw;
        }

        // Call vertex generator/placement method
        genVertices();
}

void ArcMesh::BuildRenderData(const glm::mat4& projection){

	// Copy projection matrix
	Projection = projection;

	// Generate the VAO
	glGenVertexArrays(1, &VAO);

	// Generate position buffer.
	glGenBuffers(1, &rte_Buffer);

	// Generate other vertex buffers
	glGenBuffers(3, VBO);

	// Generate EBO
	glGenBuffers(1, &EBO);
}

void ArcMesh::genVertices(){

    // Deg->Radian conversion.
	constexpr double deg_to_rad = 3.141592653589 / 180.0;


    // Lambda to convert lattitude/longitude coord pairs into xyz amounts (on the unit sphere)
    auto convert_latlon = [](const double& lattitude, const double& longitude)->glm::vec3 {
        double r = std::cos(deg_to_rad * lattitude);
        return glm::vec3(
            r * std::cosf(deg_to_rad * longitude),
            r * std::sinf(deg_to_rad * lattitude),
            r * std::sinf(deg_to_rad * longitude)
        );
    };

    // We will iterate through out lattitude/longitude range and place vertices accordingly.
    double d_lon = (EasternLimit - WesternLimit) / static_cast<double>(SubdivisionLevel);
    double d_lat = (NorthernLimit - SouthernLimit) / static_cast<double>(SubdivisionLevel);

    // Set initial lat/long
    double lon = WesternLimit;
    double lat = SouthernLimit;

    // Reserve space in vertices container
    vertices.reserve(SubdivisionLevel * SubdivisionLevel);

    // Setup vertices
    for(size_t i = 0; i < SubdivisionLevel; ++i){
        // Reset western limit during each iteration.
        lon = WesternLimit; 
        for(size_t j = 0; j < SubdivisionLevel; ++j){
            glm::vec3 pt = convert_latlon(lat, lon);
            AddVertex(lod_vertex_t(pt, glm::vec3(glm::normalize(pt) - glm::vec3(0.0f))));
            // Add current coordinate to container of used coords.
            geoPositions.push_back(glm::vec2(lat, lon));
            // Add coord + vertex pointers to vertexMap object.
            vertexMap.insert(std::make_pair(&geoPositions.back(), &vertices.back()));
            lon += d_lon; 
        }
        lat += d_lat;
    }

    // Generate triangles now.
    genTriangles();
}

void ArcMesh::genTriangles(){

    // Lambda for getting distance on the unit sphere between two points.
    auto unit_sphere_dist = [](const glm::vec3& p0, const glm::vec3& p1){
        // Normalize inputs, get distance using glm func
        return glm::distance(glm::normalize(p0), glm::normalize(p1));
    };

    

    for(size_t i = 0; i < SubdivisionLevel; ++i){
        for(size_t j = 0; j < SubdivisionLevel; ++j){
            // Get indices of square we currently want to make triangles for
            const index_t i0 = i + (j * SubdivisionLevel + 1);
            const index_t i1 = i0 + 1;
            const index_t i2 = i0 + SubdivisionLevel + 1;
            const index_t i3 = i2 + 1;

            // Get diagonal distances between corners
            double v0v3_dist = unit_sphere_dist(GetVertex(i0).Position, GetVertex(i3).Position);
            double v1v2_dist = unit_sphere_dist(GetVertex(i1).Position, GetVertex(i2).Position);

            // If distance between v1 -> v2 is shortest, create following triangles
            if(v1v2_dist < v0v3_dist){
                AddTriangle(i0, i1, i2);
                AddTriangle(i2, i1, i3);
            }
            // Otherwise, add these triangles
            else{
                AddTriangle(i0, i1, i3);
                AddTriangle(i0, i3, i2);
            }
        }
    }

}