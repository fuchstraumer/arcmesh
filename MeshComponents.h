#pragma once
#ifndef MESH_COMPONENTS_H
#define MESH_COMPONENTS_H

// Standard library includes
#include <vector>
#include <cstdint>

/*
	MESH_COMPONENTS_H

	This header contains definitions and implementations for the Mesh
	components. This includes:

	vertex_t - default vertex type
	index_t - default index type
	triangle_t - templated triangle type, template parameter is the index type. Set to default to index_t.

*/


// Index type: uint16_t would be better for the GPU, but this messes up rendering and many of my meshes
// already run into the limits of 16-bit precision. Investigate this more?
using index_t = std::uint32_t;

// Vertex struct
struct vertex_t {

	// Default constructor.
	vertex_t(glm::vec3 pos = glm::vec3(0.0f), glm::vec3 norm = glm::vec3(0.0f), glm::vec2 uv = glm::vec2(0.0f)) : Position(pos), Normal(norm), UV(uv) { }

	// Move ctor
	vertex_t(vertex_t&& other) noexcept : Position(std::move(other.Position)), Normal(std::move(other.Normal)), UV(std::move(other.UV)) {}

	// Move operator
	vertex_t& operator=(vertex_t&& other) noexcept {
		std::swap(*this, other);
		return *this;
	}

	// Default destructor. Specified so this class be used as an abstract base class.
	~vertex_t() = default;

	// Position of this vertex
	glm::vec3 Position;

	// Normal of this vertex
	glm::vec3 Normal;

	// Texture coordinate to be used for this vertex
	glm::vec2 UV;
};

struct lod_vertex_t {

	// method to convert input dvec3 into two vec3s.

	lod_vertex_t(glm::dvec3 pos = glm::dvec3(0.0), glm::vec3 norm = glm::vec3(0.0f), glm::vec2 uv = glm::vec2(0.0f), float morph_amt = 0.0f) : Normal(norm), UV(uv), MorphAmt(morph_amt), Position(pos) {
		// If pos is 0.0, skip conversion process.
		if (pos.x == 0.0 && pos.y == 0.0 && pos.z == 0.0){
			HighPos = glm::vec3(0.0f);
			LowPos = glm::vec3(0.0f);
		}
		else {
			// Convert double precision values in "pos" into two floating point vectors, representing the lower
			// and upper 32 bits of "pos".
			
			// Lambda for doing the actual conversion.
			auto convert_to_double = [](const double& input, float& out_high, float& out_low) {
				if (input >= 0.0) {
					double input_high = floor(input / 65536.0) * 65536.0;
					out_high = static_cast<float>(input_high);
					out_low = static_cast<float>(input - input_high);
					return;
				}
				else {
					double input_high = floor(-input / 65536.0) * 65536.0;
					out_high = static_cast<float>(-input_high);
					out_low = static_cast<float>(input + input_high);
					return;
				}
			};

			// Convert
			convert_to_double(pos.x, HighPos.x, LowPos.x);
			convert_to_double(pos.y, HighPos.y, LowPos.y);
			convert_to_double(pos.z, HighPos.z, LowPos.z);
		}
	}

	// Move ctor
	lod_vertex_t(lod_vertex_t&& other) noexcept : HighPos(std::move(other.HighPos)), LowPos(std::move(other.LowPos)), Normal(std::move(other.Normal)), 
		UV(std::move(other.UV)), MorphAmt(std::move(other.MorphAmt)), Position(std::move(other.Position)) {}

	// Move operator
	lod_vertex_t& operator=(lod_vertex_t&& other) noexcept {
		std::swap(*this, other);
		return *this;
	}

	glm::vec3 HighPos;

	glm::vec3 LowPos;

	glm::vec3 Normal;

	glm::vec2 UV;

	// Amount to Morph in vertical direction (from center of sphere) when moving from a lower LOD to a higher one. 
	float MorphAmt;

	// Stored at end - position used in ctor. useful in various non-rendering locations.
	glm::dvec3 Position;

};

// Triangle struct. Holds indices to member vertices. Useful for geometry queries. 
template<typename index_type = index_t>
struct triangle_t {

	// Create a triangle using the three vertex indices giving.
	triangle_t(index_type const &i0, index_type const &i1, index_type const &i2) {
		this->i0 = i0;
		this->i1 = i1;
		this->i2 = i2;
	}

	// Move ctor
	triangle_t(triangle_t&& other) noexcept : i0(std::move(other.i0)), i1(std::move(other.i1)), i2(std::move(other.i2)) {}

	// Default empty constructor
	triangle_t() { }

	// Vertex indices for this triangle
	index_type i0, i1, i2;

};


#endif // MESH_COMPONENTS_H