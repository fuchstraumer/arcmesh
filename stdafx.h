#pragma once

// Standard library includes
#include <vector>
#include <iostream>
#include <cstdint>
#include <algorithm>
#include <string>
#include <sstream>
#include <fstream>
#include <array>
#include <unordered_map>
#include <list>

// OpenGL includes, GLEW first

#define GLEW_STATIC
#include "GL\glew.h"

#define GLFW_DLL
#include "GLFW\glfw3.h"

#define GLM_SWIZZLE
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"