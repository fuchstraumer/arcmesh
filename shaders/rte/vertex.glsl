#version 430

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;
uniform mat4 normTransform;
uniform vec3 view_pos_high;
uniform vec3 view_pos_low;

layout(location = 0) in vec3 high_pos;
layout(location = 1) in vec3 low_pow;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec2 uv;
layout(location = 4) in float morph;

out vec3 vPos;
out vec3 vNorm;

void main(){
    vec4 Position = vec4(position,1.0f);
    vNormal = mat3(normTransform) * normal;
    vPosition = projection * view * model * Position;

    fPos = (model * Position).xyz;
}