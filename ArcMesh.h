#ifndef ARC_MESH_H
#define ARC_MESH_H
#include "stdafx.h"
#include "MeshComponents.h"
#include "util\Shader.h"
/*

    Mesh of a region of the surface of a globe, selectively subdividable
    upon construction. Lattitude/Longitude ranges in ctor define positions
    of generated vertices, initially all mapped to the unit sphere.

*/


class ArcMesh {
    // Delete copy operators.
    ArcMesh(const ArcMesh& other) = delete;
    ArcMesh& operator=(const ArcMesh& other) = delete;
    // TODO: Define move operators.
public:

    ArcMesh(const size_t &subdivision_level, const double& western_bound, const double& eastern_bound, const double& southern_bound, const double& northern_bound);

    // "LOD" level, which also defines number of vertices along a single edge of the generated mesh.
    size_t SubdivisionLevel;

    // Bounds of this object, in geographical terms (degrees).
    double WesternLimit, EasternLimit, SouthernLimit, NorthernLimit;

    // Override of render data builder
    void BuildRenderData(const glm::mat4& projection);

    void Render(const ShaderProgram& shader, const glm::mat4& view, const glm::dvec3& view_pos);

    // Getter methods for mesh elements.
    lod_vertex_t& GetVertex(const index_t& idx);
    index_t& GetIndex(const index_t& idx);

    // Setter methods for mesh elements.
    void SetVertex(const index_t& idx, const lod_vertex_t& vert);
    void SetVertex(const index_t& idx, lod_vertex_t&& vert);

    // Adder methods. Returns index to newly added element.
    void AddVertex(const lod_vertex_t& vert);
    void AddVertex(lod_vertex_t&& vert);
    void AddTriangle(const index_t& i0, const index_t& i1, const index_t& i2);
    void AddTriangle(index_t&& i0, index_t&& i1, index_t&& i2);

    // Positional attributs
    glm::vec3 Position;
    glm::vec3 Scale;
    glm::vec3 Angle;

    // Matrices used when rendering.
	glm::mat4 Projection, Model;

private:

    // Vertex positions relative-to-eye. Updated every frame. 
    GLuint rte_Buffer;

	// VBO, but for this object contains everything but the actual positions.
	GLuint VBO[3];

	// VAO and EBO, unchanged from usual setup
	GLuint VAO, EBO;

    // Generate vertices. called from ctor.
    void genVertices();

    // Generate triangles/indices by winding vertices. called from vertice generation method.
    void genTriangles();

    // Map between lat/lon positions and vertices, making it much easier to access and modify vertices later.
    std::unordered_map<glm::vec2*, lod_vertex_t*> vertexMap;

    // Vector of lat/lon positions in this mesh
    std::vector<glm::vec2> geoPositions;

    // vector of double-precision positions.
    std::vector<glm::dvec3> doublePositions;

    // vector of floating-point positions.
    std::vector<glm::vec3> floatPositions;

    // Vertex container
    std::vector<lod_vertex_t> vertices;

    // Index container
    std::vector<index_t> indices;

    // Final MVP matrix in relative-to-center format.
    glm::mat4 mvp;

};

#endif // !ARC_MESH_H